<?php
/**
 +---------------------------------------------------------- 
 * date: 2018-05-03 11:08:42
 +---------------------------------------------------------- 
 * author: Raoxiaoya
 +---------------------------------------------------------- 
 * describe: 游戏记录处理
 +---------------------------------------------------------- 
 */

set_time_limit(0);
require_once('lib/common.php');

function main_run() {
	while(true)
	{
		$action_game_record = new action_game_record();
		$action_game_record->main();
		sleep(2);
	}
}

main_run();