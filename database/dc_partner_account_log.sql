/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.210
Source Server Version : 50713
Source Host           : 192.168.1.210:3306
Source Database       : dc_u3d_king

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2018-08-22 13:49:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dc_partner_account_log
-- ----------------------------
DROP TABLE IF EXISTS `dc_partner_account_log`;
CREATE TABLE `dc_partner_account_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `log_money_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '钱的类型金币 1人民币',
  `log_partner_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '代理ID',
  `log_bef_money` int(11) NOT NULL DEFAULT '0' COMMENT '变动前金额：分',
  `log_money` int(11) NOT NULL DEFAULT '0' COMMENT '变动金额：分',
  `log_aft_money` int(11) NOT NULL DEFAULT '0' COMMENT '变动后金额：分',
  `log_add_time` int(10) NOT NULL DEFAULT '0' COMMENT 'add time',
  `log_type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '1-渠道收益月进账 \r\n2-推广收益进账,\r\n3-代理收益日进账,\r\n4-提现出账',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='资金流水';
