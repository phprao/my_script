-- 停脚本 script_player_earn.php

-- dc_agent_super_income_config
alter table dc_agent_super_income_config add super_share_ext int(11) NOT NULL DEFAULT '0' COMMENT 'e额外分成比例 万分之';
--1	0	0	    <=	1000	0
--2	0	150000	<=	1000	100
--3	0	300000	<=	1000	200
--4	0	500000	<=	1000	300
--5	0	800000	<=	1000	400
--6	0	1000000	<=	1000	500

create table dc_agent_super_statistics_date_store like dc_agent_super_statistics_date ;
insert into dc_agent_super_statistics_date_store select * from dc_agent_super_statistics_date ;

alter table dc_agent_super_statistics_date add statistics_month int(11) DEFAULT '0';

delete from dc_agent_super_statistics_date where statistics_time < 1533052800;


DROP TABLE IF EXISTS `dc_agent_super_statistics_date_ext`;
CREATE TABLE `dc_agent_super_statistics_date_ext` (
  `statistics_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `statistics_agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '特代ID',
  `statistics_money_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '货币类型：1-金币',
  `statistics_money_data_direct` bigint(20) NOT NULL DEFAULT '0' COMMENT '旗下直属星级推广员的金币消耗，个',
  `statistics_money_data` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '除直属星级推广员之外的全部玩家的金币消耗，个',
  `statistics_date` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '统计时间 2018-01',
  `statistics_time` int(11) DEFAULT '0' COMMENT '时间戳',
  `statistics_super_share_direct` int(11) DEFAULT '0' COMMENT '旗下直属星级推广员的金币消耗 的提成比例  %万分之',
  `statistics_super_share` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '除直属星级推广员之外的分成比例 %万分之',
  `statistics_super_share_ext` int(11) DEFAULT '0' COMMENT '额外收益比例',
  `statistics_money_rate_value` int(11) NOT NULL DEFAULT '1' COMMENT '金币与人民币兑换比例',
  `statistics_money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '日结总计：分',
  `statistics_money_ext` int(11) DEFAULT '0' COMMENT '月度奖金：分',
  `statistics_money_all` int(11) DEFAULT '0' COMMENT '总收益：分',
  `statistics_add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '统计添加时间',
  PRIMARY KEY (`statistics_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='渠道月度奖金';


-- 编辑阶梯分成

-- 执行脚本 script_player_earn.php

-- 执行脚本 script_channel_reward.php