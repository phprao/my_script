/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.210
Source Server Version : 50713
Source Host           : 192.168.1.210:3306
Source Database       : dc_u3d_king

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2018-08-22 14:41:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dc_partner
-- ----------------------------
DROP TABLE IF EXISTS `dc_partner`;
CREATE TABLE `dc_partner` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(255) DEFAULT NULL,
  `login_user_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT '0' COMMENT '对应的渠道id',
  `third_share_rate` float(11,4) DEFAULT '0.5000' COMMENT '第三方平台分成',
  `share_rate` float(11,4) DEFAULT '0.3000' COMMENT '渠道分成',
  `status` tinyint(3) DEFAULT '1' COMMENT '状态：1-开启，2-关闭',
  `remark` varchar(255) DEFAULT '',
  `create_time` int(11) DEFAULT '0',
  `create_date` varchar(20) DEFAULT '',
  PRIMARY KEY (`partner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8 COMMENT='渠道合作商';
