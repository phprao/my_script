/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.210
Source Server Version : 50713
Source Host           : 192.168.1.210:3306
Source Database       : dc_u3d_king

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2018-08-23 16:01:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dc_robot_data
-- ----------------------------
DROP TABLE IF EXISTS `dc_robot_data`;
CREATE TABLE `dc_robot_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) DEFAULT '0',
  `game_name` varchar(255) DEFAULT '',
  `room_id` int(11) DEFAULT '0',
  `room_name` varchar(255) DEFAULT '',
  `time` int(11) DEFAULT '0',
  `date` varchar(20) DEFAULT '',
  `change_money_total` varchar(255) DEFAULT '0',
  `add_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机器人输赢按日统计';
