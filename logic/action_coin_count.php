<?php

class action_coin_count
{
	private $mysql;
	private $logs;
	private $logTag = 'action_coin_count';
	private $statistics_table = 'dc_statistics_total';

	public function __construct(){
		$this->mysql   	= new MysqlDriver(Config::$mysql_config);
		$this->logs    	= new logger();
	}

	public function main(){
		$start = time();

        $this->date = date('Y-m-d', $start);
        $this->time = strtotime($this->date);
		// 清空原有数据
		$result = $this->clear_last_coin_num();
		if(!$result){
			$this->logs->error($this->logTag,'function clear_last_coin_num failed ...');
			return true;
		}

		$players = $this->mysql->find("select player_id from dc_player_info");
		while(count($players))
		{
			$player = array_pop($players);
			$info = $this->mysql->find("select player_coins,player_agent_id from dc_player_info where player_id = " . $player['player_id'].' limit 1');
			if($info[0]['player_coins'] <= 0){
				continue;
			}
			$re = $this->set_last_coin_num($player['player_id'], $info[0]['player_coins'], $info[0]['player_agent_id']);
			if(!$re){
               	$this->logs->error($this->logTag,'统计金币数失败：player_id='.$player['player_id'].' | player_coins='.$info[0]['player_coins']);
               	break;
            }
		}

        $this->mysql->close();

		$end = time();
		$this->logs->info($this->logTag,'耗时: ' . ($end - $start) . ' seconds');
	}

	public function clear_last_coin_num(){
        $sql = "update dc_statistics_total set statistics_sum = 0 where statistics_mode = 4 and statistics_type = 2 and statistics_timestamp = ".$this->time;
        $re = $this->mysql->query($sql);
        return $re;
	}

	public function set_last_coin_num($player_id, $player_coins, $agent_id){
		// 总公司, 推广员
        $role_arr = [0,1];
        $type = 2;// day
        $mode = 4;// 剩余金币数
        $time = time();

        foreach($role_arr as $val){
            $where = [
                'statistics_role_type'  =>$val,
                'statistics_role_value' =>0,
                'statistics_type'       =>$type
            ];
            if($val == 1){
                $where['statistics_role_value'] = $agent_id;
            }
			$where['statistics_mode']       = $mode;
			$where['statistics_sum']        = $player_coins;
			$where['statistics_timestamp']  = $this->time;
			$where['statistics_datetime']   = $this->date;
			$where['statistics_update']     = date('Y-m-d H:i:s',$time);
			$where['statistics_time']       = $time;
            // 是否已经存在
            $w = [
                'statistics_role_type'  =>$where['statistics_role_type'],
                'statistics_role_value' =>$where['statistics_role_value'],
                'statistics_mode'       =>$where['statistics_mode'],
                'statistics_type'       =>$where['statistics_type'],
                'statistics_timestamp'  =>$where['statistics_timestamp'],
            ];
            $record = $this->mysql->select($this->statistics_table, '*', $w, 'limit 1');
            if($record){
                $sql = "UPDATE ".$this->statistics_table." SET statistics_sum = statistics_sum + ".$where['statistics_sum'].",statistics_update = '".$where['statistics_update']."' where statistics_id = ".$record[0]['statistics_id'];
		        $re = $this->mysql->query($sql);
            }else{
                $re = $this->mysql->insert($this->statistics_table, $where);
            }

            if(!$re){
            	$this->logs->error($this->logTag,'表'.$this->statistics_table.' 更新失败！');
            	return false;
            }
        }

        return true;
	}
}
