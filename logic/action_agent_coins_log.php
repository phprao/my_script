<?php

class action_agent_coins_log
{
	private $mysql;
	private $logs;
	private $logTag = 'action_agent_coins_log';
	private $limit = 50;
	private $statistics_table = 'dc_statistics_total';

	public function __construct(){
		$this->mysql   	= new MysqlDriver(Config::$mysql_config);
		$this->logs    	= new logger();
	}

	public function main(){
		$start = time();

		$list = $this->getList($start);
        if(empty($list)){
        	return true;
        }

		foreach($list as $val){
			$this->deal_record($val);
		}

		$end = time();
		$this->logs->info($this->logTag,'耗时: ' . ($end - $start) . ' seconds');
	}

	protected function getList($start){
		$sql = "SELECT * from dc_agent_coins_log where update_time <= ".($start - 86400)." order by id asc limit ".$this->limit;
        $list = $this->mysql->find($sql);

        return $list;
	}

	protected function deal_record($record){
		$this->mysql->query('START TRANSACTION');

		// 统计
		$res1 = $this->set_last_coin_num($record);
		// 复制
		$res2 = $this->copy_log($record);
		// 删除
		$res3 = $this->del_log($record);

		if($res1){
            $this->mysql->query('COMMIT');
            return true;
        }else{
            $this->logs->info($this->logTag, "deal_record失败: res1 = $res1, res2 = $res2, res3 = $res3");
            $this->mysql->query('ROLLBACK');
            $this->update_time($record['id']);
            return false;
        }
	}

	protected function set_last_coin_num($record){
		// 0总公司, 1推广员
        $role = 0;
        $role_value = 0;
        // 1-小时，2-天，3-从开始总计
        $type_arr = [1,2,3];
        // 5-上分数，6-下分数
        $mode = 5;

        if($record['change_coin'] == 0){
        	return true;
        }

        $log_time = $record['time'];
        $time = time();

        $user_type = $this->get_user_type($record['action_user']);
        if($user_type == 2){
        	$role = 1;
        	$agent_id = $this->get_agent($record['action_user']);
        	if(!$agent_id){
        		$this->logs->info($this->logTag, "dc_agent_info表中数据有误，agent_user_id=".$record['action_user']);
        		return false;
        	}
        	$role_value = $agent_id;
        	// 推广员操作：>0下分，<0上分
        	if($record['change_coin'] > 0){
	        	$mode = 6;
	        }
        }elseif($user_type == 1){
        	// 总后台操作：>0上分，<0下分
        	if($record['change_coin'] < 0){
	        	$mode = 6;
	        }
        }else{
        	$this->logs->info($this->logTag, "users表中数据有误，id=".$record['action_user']);
        	return false;
        }

        $record['change_coin'] = abs($record['change_coin']);

        foreach($type_arr as $val){
            $where = [
                'statistics_role_type'  =>$role,
                'statistics_role_value' =>$role_value,
                'statistics_type'       =>$val
            ];
            if($val == 1){
                $where['statistics_timestamp']  = strtotime(date('Y-m-d H:0:0', $log_time));
                $where['statistics_datetime']   = date('Y-m-d H', $log_time);
            }
            if($val == 2){
                $where['statistics_timestamp']  = strtotime(date('Y-m-d', $log_time));
                $where['statistics_datetime']   = date('Y-m-d', $log_time);
            }
            if($val == 3){
                $where['statistics_timestamp']  = 0;
                $where['statistics_datetime']   = 0;
            }
			$where['statistics_mode']       = $mode;
			$where['statistics_sum']        = $record['change_coin'];
			$where['statistics_update']     = date('Y-m-d H:i:s',$time);
			$where['statistics_time']       = $time;
            // 是否已经存在
            $w = [
                'statistics_role_type'  =>$where['statistics_role_type'],
                'statistics_role_value' =>$where['statistics_role_value'],
                'statistics_mode'       =>$where['statistics_mode'],
                'statistics_type'       =>$where['statistics_type'],
                'statistics_timestamp'  =>$where['statistics_timestamp'],
            ];
            $log = $this->mysql->select($this->statistics_table, '*', $w, 'limit 1');
            if($log){
                $sql = "UPDATE ".$this->statistics_table." SET statistics_sum = statistics_sum + ".$where['statistics_sum'].",statistics_update = '".$where['statistics_update']."' where statistics_id = ".$log[0]['statistics_id'];
		        $re = $this->mysql->query($sql);
            }else{
                $re = $this->mysql->insert($this->statistics_table, $where);
            }

            if(!$re){
            	$this->logs->error($this->logTag,'表'.$this->statistics_table.' 更新失败！');
            	return false;
            }
        }

        return true;
	}

	protected function copy_log($record){
		$record['log_id'] = $record['id'];
        unset($record['id']);
		return $this->mysql->insert('dc_agent_coins_log_record', $record);
	}

	protected function del_log($record){
		return $this->mysql->query('delete from dc_agent_coins_log where id = '.$record['id']);
	}

	protected function get_user_type($user_id){
		$data = $this->mysql->find('select * from dc_users where id = '.$user_id.' limit 1');
		if(!$data){
			return 0;
		}

		return $data[0]['user_type'];
	}

	protected function get_agent($user_id){
		$data = $this->mysql->find('select * from dc_agent_info where agent_user_id = '.$user_id.' limit 1');
		if(!$data){
			return 0;
		}

		return $data[0]['agent_id'];
	}

	protected function update_time($id){
		return $this->mysql->query('update dc_agent_coins_log set update_time = '.time().' where id = '.$id);
	}
}
