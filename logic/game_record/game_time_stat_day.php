<?php

class game_time_stat_day
{
	private $mysql;
	private $logs;
	private $logTag = 'game_time_stat_day';

	public function __construct($param = []){
		$this->mysql = $param['mysql'];
		$this->logs  = $param['logs'];
	}

	public function main($record, $agent_id){
		$log_time = $record['game_record_time'];
        $time = time();

        $agent_arr = [0, $agent_id];

        foreach($agent_arr as $agent){
        	$w = [
				'game_id'  =>$record['game_record_game_id'],
				'agent_id' =>$agent,
				'time'     =>strtotime(date('Y-m-d', $log_time))
        	];
            $time_long = $record['game_record_game_over_time'] - $record['game_record_begin_time'];

        	$log = $this->mysql->select('dc_game_stat_day', '*', $w, 'limit 1');
            if($log){
                $sql = "UPDATE dc_game_stat_day SET time_long = time_long + ".$time_long." where id = ".$log[0]['id'];
		        $re = $this->mysql->query($sql);
            }else{
				$w['game_name']  = $this->get_game_name($w['game_id']);
				$w['time_long']  = $time_long;
				$w['time']       = strtotime(date('Y-m-d', $log_time));
				$w['date']       = date('Y-m-d', $log_time);
				$w['createtime'] = time();
                $re = $this->mysql->insert('dc_game_stat_day', $w);
            }

            if(!$re){
            	$this->logs->error($this->logTag,'表dc_game_stat_day更新失败！');
            	return false;
            }

        }
        
		return true;
	}

	protected function get_game_name($game_id){
        $data = $this->mysql->find('select * from dc_game_info where game_id = '.$game_id.' limit 1');
        if(!$data){
            return '--';
        }

        return $data[0]['game_name'];
    }
}