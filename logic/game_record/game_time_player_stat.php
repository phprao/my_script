<?php

class game_time_player_stat
{
	private $mysql;
	private $logs;
	private $logTag = 'game_time_player_stat';

	public function __construct($param = []){
		$this->mysql = $param['mysql'];
		$this->logs  = $param['logs'];
	}

	public function main($record){
		// 2-玩家
        $role = 2;
        // 2-天
        $type = 2;
        // 7-单个玩家游戏时长-天
        $mode = 7;

        $log_time = $record['game_record_time'];
        $time = time();

        $where = [
            'statistics_role_type'  =>$role,
            'statistics_role_value' =>$record['game_record_player_id'],
            'statistics_type'       =>$type,
            'statistics_mode'		=>$mode,
            'statistics_sum'		=>$record['game_record_game_over_time'] - $record['game_record_begin_time'],
            'statistics_update'		=>date('Y-m-d H:i:s',$time),
            'statistics_time'		=>$time,
            'statistics_timestamp'	=>strtotime(date('Y-m-d', $log_time)),
            'statistics_datetime'	=>date('Y-m-d', $log_time)
        ];

        // 是否已经存在
        $w = [
            'statistics_role_type'  =>$where['statistics_role_type'],
            'statistics_role_value' =>$where['statistics_role_value'],
            'statistics_mode'       =>$where['statistics_mode'],
            'statistics_type'       =>$where['statistics_type'],
            'statistics_timestamp'  =>$where['statistics_timestamp'],
        ];
        $log = $this->mysql->select('dc_statistics_total', '*', $w, 'limit 1');
        if($log){
            $sql = "UPDATE dc_statistics_total SET statistics_sum = statistics_sum + ".$where['statistics_sum'].",statistics_update = '".$where['statistics_update']."' where statistics_id = ".$log[0]['statistics_id'];
	        $re = $this->mysql->query($sql);
        }else{
            $re = $this->mysql->insert('dc_statistics_total', $where);
        }

        if(!$re){
        	$this->logs->error($this->logTag,'表dc_statistics_total更新失败！');
        	return false;
        }

        return true;
	}

}