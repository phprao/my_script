<?php

class action_clear_token
{
	private $redis;
	private $logs;
	private $tag = ['admin:login:token','h5:login:token'];
	private $logTag = 'action_clear_token';

	public function __construct(){
		$this->logs    	= new logger();
	}

	public function main(){
		$start = time();
		
		$this->redis = new RedisDriver(0);
		foreach($this->tag as $tag){
			$prefix =  $tag . ':*';
			$tokens = $this->redis->handler->keys($prefix);
			while(count($tokens))
			{
				$key = array_pop($tokens);
				// 访问的时候，如果key过期，redis会自动清除该key
				$this->redis->handler->get($key);
			}
		}

		$this->redis->deinitDataRedis();

		$end = time();
		$this->logs->info($this->logTag,'耗时: ' . ($end - $start) . ' seconds');
	}
}
