<?php

class game_player_stat
{
	private $mysql;
	private $logs;
	private $logTag = 'game_player_stat';

	public function __construct($param = []){
		$this->mysql = $param['mysql'];
		$this->logs  = $param['logs'];
	}

	public function main($record, $agent_id){
		// 判断玩家是否已经统计了
		if($this->is_have_stat($record)){
			return true;
		}

		// 0总公司, 1推广员
        $role_arr = [0,1];
        // 1-小时，2-天，3-从开始总计
        $type_arr = [1,2,3];
        // 3-游戏玩家数
        $mode = 3;

        $log_time = $record['change_money_time'];
        $time = time();

        foreach($type_arr as $type){
        	foreach($role_arr as $role){
	            $where = [
	                'statistics_role_type'  =>$role,
	                'statistics_role_value' =>0,
	                'statistics_type'       =>$type,
	                'statistics_mode'		=>$mode,
	                'statistics_sum'		=>1,
	                'statistics_update'		=>date('Y-m-d H:i:s',$time),
	                'statistics_time'		=>$time
	            ];

	            if($type == 1){
	                $where['statistics_timestamp']  = strtotime(date('Y-m-d H:0:0', $log_time));
	                $where['statistics_datetime']   = date('Y-m-d H', $log_time);
	            }
	            if($type == 2){
	                $where['statistics_timestamp']  = strtotime(date('Y-m-d', $log_time));
	                $where['statistics_datetime']   = date('Y-m-d', $log_time);
	            }
	            if($type == 3){
	                $where['statistics_timestamp']  = 0;
	                $where['statistics_datetime']   = 0;
	            }

	            if($role == 1){
	            	$where['statistics_role_value'] = $agent_id;
	            }

	            // 是否已经存在
	            $w = [
	                'statistics_role_type'  =>$where['statistics_role_type'],
	                'statistics_role_value' =>$where['statistics_role_value'],
	                'statistics_mode'       =>$where['statistics_mode'],
	                'statistics_type'       =>$where['statistics_type'],
	                'statistics_timestamp'  =>$where['statistics_timestamp'],
	            ];
	            $log = $this->mysql->select('dc_statistics_total', '*', $w, 'limit 1');
	            if($log){
	                $sql = "UPDATE dc_statistics_total SET statistics_sum = statistics_sum + ".$where['statistics_sum'].",statistics_update = '".$where['statistics_update']."' where statistics_id = ".$log[0]['statistics_id'];
			        $re = $this->mysql->query($sql);
	            }else{
	                $re = $this->mysql->insert('dc_statistics_total', $where);
	            }

	            if(!$re){
	            	$this->logs->error($this->logTag,'表dc_statistics_total更新失败！');
	            	return false;
	            }
	        }
        }

        return true;
	}

	protected function is_have_stat($record){
		$log_start = strtotime(date('Ymd', $record['change_money_time']));
        $log_end = $log_start + 86400;
        $data = $this->mysql->find('select * from dc_player_change_log where change_money_type = 2 and change_money_player_id = '.$record['change_money_player_id'].' and change_money_time >= '.$log_start.' and change_money_time < '.$log_end.' limit 1');
        if($data){
        	return true;
        }else{
        	return false;
        }
	}
}