<?php

class game_stat_day
{
	private $mysql;
	private $logs;
	private $logTag = 'game_stat_day';

	public function __construct($param = []){
		$this->mysql = $param['mysql'];
		$this->logs  = $param['logs'];
	}

	public function main($record, $agent_id){
		// 判断玩家是否已经统计了
		if($this->is_have_stat($record)){
			return true;
		}

		$log_time = $record['change_money_time'];
        $time = time();

        $agent_arr = [0, $agent_id];

        foreach($agent_arr as $agent){
        	$w = [
				'game_id'  =>$record['change_money_game_id'],
				'agent_id' =>$agent,
				'time'     =>strtotime(date('Y-m-d', $log_time))
        	];
        	$log = $this->mysql->select('dc_game_stat_day', '*', $w, 'limit 1');
            if($log){
                $sql = "UPDATE dc_game_stat_day SET num = num + 1 where id = ".$log[0]['id'];
		        $re = $this->mysql->query($sql);
            }else{
				$w['game_name']  = $this->get_game_name($w['game_id']);
				$w['num']        = 1;
				$w['time']       = strtotime(date('Y-m-d', $log_time));
				$w['date']       = date('Y-m-d', $log_time);
				$w['createtime'] = time();
                $re = $this->mysql->insert('dc_game_stat_day', $w);
            }

            if(!$re){
            	$this->logs->error($this->logTag,'表dc_game_stat_day更新失败！');
            	return false;
            }

        }
        
		return true;
	}

	protected function is_have_stat($record){
		$log_start = strtotime(date('Ymd', $record['change_money_time']));
        $log_end = $log_start + 86400;
        $data = $this->mysql->find('select * from dc_player_change_log where change_money_tax = 2 and change_money_game_id = '.$record['change_money_game_id'].' and change_money_player_id = '.$record['change_money_player_id'].' and change_money_time >= '.$log_start.' and change_money_time < '.$log_end.' limit 1');
        if($data){
        	return true;
        }else{
        	return false;
        }
	}

	protected function get_game_name($game_id){
        $data = $this->mysql->find('select * from dc_game_info where game_id = '.$game_id.' limit 1');
        if(!$data){
            return '--';
        }

        return $data[0]['game_name'];
    }
}