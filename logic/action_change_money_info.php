<?php

class action_change_money_info
{
	private $mysql;
	private $logs;
	private $logTag = 'action_change_money_info';
	private $limit = 50;

	public function __construct(){
		$this->mysql   	= new MysqlDriver(Config::$mysql_config);
		$this->logs    	= new logger();
	}

	public function main(){
		$start = time();

		$list = $this->getList($start);
        if(empty($list)){
        	return true;
        }
        $flag = true;
		foreach($list as $val){
			$result = $this->deal_record($val);
            if (!$result) {
                $flag = false;
            }
		}
        if (!$flag) {
            $this->logs->info($this->logTag,'部分记录处理失败');
        }

		$end = time();
		$this->logs->info($this->logTag,'耗时: ' . ($end - $start) . ' seconds');
	}

	protected function getList($start){
		$sql = "SELECT * from dc_change_money_info where change_money_update_time <= ".($start - 86400)." order by change_money_id asc limit ".$this->limit;
        $list = $this->mysql->find($sql);

        return $list;
	}

	protected function deal_record($record){
		$this->mysql->query('START TRANSACTION');

		// 统计
		$res1 = $this->do_data($record);
		// 复制
		$res2 = $this->copy_log($record);
		// 删除
		$res3 = $this->del_log($record);

		if($res1){
            $this->mysql->query('COMMIT');
            return true;
        }else{
            $this->logs->info($this->logTag, "deal_record失败: res1 = $res1, res2 = $res2, res3 = $res3");
            $this->mysql->query('ROLLBACK');
            $this->update_time($record['change_money_id']);
            return false;
        }
	}

    /**
     * 数据分流统计
     */
    protected function do_data($record){
        $type = $record['change_money_type'];
        $agent_id = $this->get_agent($record['change_money_player_id']);
        if(!$agent_id){
            $this->logs->info($this->logTag, "dc_player_info表中缺少代理，player_id=".$record['change_money_player_id']);
            return false;
        }
        $param = [
            'logs' =>$this->logs,
            'mysql'=>$this->mysql,
        ];

        if($type == 4){
            // 新增用户数
            $r1 = (new add_player_stat($param))->main($record, $agent_id);
        }elseif($type == 2){
            // 每日游戏玩家数
            $r11 = (new game_player_stat($param))->main($record, $agent_id);
            // 各游戏的日统计
            $r12 = (new game_stat_day($param))->main($record, $agent_id);

            $r1 = $r11 && $r12;
        }else{
            $r1 = true;
        }
        // 备份到玩家记录
        $r2 = $this->player_change_log($record, $agent_id);

        return $r1 && $r2;
    }

	protected function player_change_log($record, $agent_id){
        $record['change_money_game_name'] = '--';
        if($record['change_money_type'] == 2){
            $record['change_money_game_name'] = $this->get_game_name($record['change_money_game_id']);
        }
        unset($record['change_money_id']);
        unset($record['change_money_update_time']);
        $record['change_money_parent_agent_id'] = $agent_id;

        return $this->mysql->insert('dc_player_change_log', $record);
    }

    protected function get_game_name($game_id){
        $data = $this->mysql->find('select * from dc_game_info where game_id = '.$game_id.' limit 1');
        if(!$data){
            return '--';
        }

        return $data[0]['game_name'];
    }

	protected function copy_log($record){
		unset($record['change_money_id']);
		return $this->mysql->insert('dc_change_money_info_record', $record);
	}

	protected function del_log($record){
		return $this->mysql->query('delete from dc_change_money_info where change_money_id = '.$record['change_money_id']);
	}

	protected function get_agent($player_id){
		$data = $this->mysql->find('select * from dc_player_info where player_id = '.$player_id.' limit 1');
		if(!$data){
			return 0;
		}

		return $data[0]['player_agent_id'];
	}

	protected function update_time($id){
		return $this->mysql->query('update dc_change_money_info set change_money_update_time = '.time().' where change_money_id = '.$id);
	}
}
