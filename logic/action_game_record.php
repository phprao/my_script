<?php

class action_game_record
{
	private $mysql;
	private $logs;
	private $logTag = 'action_game_record';
	private $limit = 50;

	public function __construct(){
		$this->mysql   	= new MysqlDriver(Config::$mysql_config);
		$this->logs    	= new logger();
	}

	public function main(){
		$start = time();

		$list = $this->getList($start);
        if(empty($list)){
        	return true;
        }
        $flag = true;
		foreach($list as $val){
			$result = $this->deal_record($val);
            if (!$result) {
                $flag = false;
            }
		}
        if (!$flag) {
            $this->logs->info($this->logTag,'部分记录处理失败');
        }

		$end = time();
		$this->logs->info($this->logTag,'耗时: ' . ($end - $start) . ' seconds');
	}

	protected function getList($start){
		$sql = "SELECT * from dc_game_record where game_record_update_time <= ".($start - 86400)." order by game_record_id asc limit ".$this->limit;
        $list = $this->mysql->find($sql);

        return $list;
	}

	protected function deal_record($record){
		$this->mysql->query('START TRANSACTION');

		// 统计
		$res1 = $this->do_data($record);
		// 复制
		$res2 = $this->copy_log($record);
		// 删除
		$res3 = $this->del_log($record);

		if($res1){
            $this->mysql->query('COMMIT');
            return true;
        }else{
            $this->logs->info($this->logTag, "deal_record失败: res1 = $res1, res2 = $res2, res3 = $res3");
            $this->mysql->query('ROLLBACK');
            $this->update_time($record['game_record_id']);
            return false;
        }
	}

    /**
     * 数据分流统计
     */
    protected function do_data($record){
        $agent_id = $this->get_agent($record['game_record_player_id']);
        if(!$agent_id){
            $this->logs->info($this->logTag, "dc_player_info表中缺少代理，player_id=".$record['game_record_player_id']);
            return false;
        }
        $param = [
            'logs' =>$this->logs,
            'mysql'=>$this->mysql,
        ];
        
        if($record['game_record_begin_time'] && $record['game_record_game_over_time'] && ($record['game_record_game_over_time'] - $record['game_record_begin_time']) > 0){

        	// 单个游戏的时长日统计
	        $r1 = (new game_time_stat_day($param))->main($record, $agent_id);
	        // 玩家游戏时长日统计
        	$r2 = (new game_time_player_stat($param))->main($record);

        }else{
        	$r1 = true;
        	$r2 = true;
        }

        return $r1 && $r2;
    }

	protected function copy_log($record){
		unset($record['game_record_id']);
		unset($record['game_record_update_time']);
		$record['game_record_time_long'] = 0;
		if($record['game_record_begin_time'] && $record['game_record_game_over_time'] && ($record['game_record_game_over_time'] - $record['game_record_begin_time']) > 0){
			$record['game_record_time_long'] = $record['game_record_game_over_time'] - $record['game_record_begin_time'];
		}
		$record['game_record_game_name'] = $this->get_game_name($record['game_record_game_id']);
		$record['game_record_agent_id'] = $this->get_agent($record['game_record_player_id']);
		return $this->mysql->insert('dc_game_record_log', $record);
	}

	protected function get_game_name($game_id){
        $data = $this->mysql->find('select * from dc_game_info where game_id = '.$game_id.' limit 1');
        if(!$data){
            return '--';
        }

        return $data[0]['game_name'];
    }

	protected function del_log($record){
		return $this->mysql->query('delete from dc_game_record where game_record_id = '.$record['game_record_id']);
	}

	protected function get_agent($player_id){
		$data = $this->mysql->find('select * from dc_player_info where player_id = '.$player_id.' limit 1');
		if(!$data){
			return 0;
		}

		return $data[0]['player_agent_id'];
	}

	protected function update_time($id){
		return $this->mysql->query('update dc_game_record set game_record_update_time = '.time().' where game_record_id = '.$id);
	}
}