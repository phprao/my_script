<?php

set_exception_handler('output_exception');
set_error_handler('output_error');
register_shutdown_function('output_shutdown');
spl_autoload_register('my_autoload');

require('lib/logger.php');
require('lib/config.php');
require('lib/mysql.php');
require('lib/redis.php');
define('LOGIC_DIR', dirname(dirname(__FILE__)) . '/logic');
$autoload_dir = [
    LOGIC_DIR,
    LOGIC_DIR.'/change_money_info',
    LOGIC_DIR.'/game_record',
];

function output_exception($e){
    $logs = new logger('error');
    $msg = $e->getFile() . ' | lime ' . $e->getLine() . ' | ' . $e->getMessage();
    $logs->error('sys_exception', $msg);
}
function output_error($errno, $errstr, $errfile, $errline){
    $logs = new logger('error');
    $msg = $errfile . ' | lime ' . $errline . ' | ' . $errstr . ' | errno ' . $errno;
    $logs->error('sys_error', $msg);
}
function output_shutdown(){
    $logs = new logger('error');
    $data = error_get_last();
    if(isset($data['message']) && !is_null($data['message'])) {
        $msg = $data['file'] . ' | line ' . $data['line'] . ' | ' . $data['message'];
        $logs->fatal('sys_shutdown', $msg);
    }
}

function my_autoload($class_name){
    global $autoload_dir;
    foreach($autoload_dir as $d){
        $full = $d . '/' . $class_name;
        if (file_exists($full . '.php')) { 
            require_once $full . '.php'; 
            break;
        }
    }
}

?>