<?php
/**
 +---------------------------------------------------------- 
 * date: 2018-05-03 11:08:42
 +---------------------------------------------------------- 
 * author: Raoxiaoya
 +---------------------------------------------------------- 
 * describe: 清除过期的token
 +---------------------------------------------------------- 
 */

set_time_limit(0);
require_once('lib/common.php');

function main_run() {
	while(true)
	{
		$action_clear_token = new action_clear_token();
		$action_clear_token->main();
		sleep(3600);
	}
}

main_run();



